#[macro_use]
extern crate lazy_static;
extern crate regex;

use regex::Regex;

fn invert(input: &str) -> String {
    lazy_static! {
        static ref NEGATIVES: Regex = Regex::new(r"^T-(.*)$").unwrap();
        static ref POSITIVES: Regex = Regex::new(r"^T(.*)$").unwrap();
    }

    if let Some(caps) = NEGATIVES.captures(&input) {
        String::from("T") + caps.get(1).unwrap().as_str()
    } else if let Some(caps) = POSITIVES.captures(&input) {
        String::from("T-") + caps.get(1).unwrap().as_str()
    } else {
        String::from(input)
    }
}

fn main() {
    loop {
        let mut input = String::new();
        match std::io::stdin().read_line(&mut input) {
            Ok(n) => {
                if n > 0 {
                    println!("{}", invert(&input.trim()))
                } else {
                    break;
                }
            }
            Err(e) => panic!("Error reading input: {}", e),
        }
    }
}

#[test]
fn it_works() {
    assert_eq!("T-123", invert("T123"));
    assert_eq!("T123", invert("T-123"));
    assert_eq!("passthrough", invert("passthrough"));
}
